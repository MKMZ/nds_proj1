"""HTTP Server

This module contains a HTTP server
"""

import threading
import socket
from parser import RequestParser

class ConnectionHandler(threading.Thread):
    """Connection Handler for HTTP Server"""

    def __init__(self, conn_socket, addr, timeout):
        """Initialize the HTTP Connection Handler
        
        Args:
            conn_socket (socket): socket used for connection with client
            addr (str): ip address of client
            timeout (int): seconds until timeout
        """
        super(ConnectionHandler, self).__init__()
        self.daemon = True
        self.conn_socket = conn_socket
        self.addr = addr
        self.timeout = timeout
        self.parser = RequestParser()
    
    def handle_connection(self):
        """Handle a new connection"""
        self.request = self.conn_socket.recv(1024)
        print "Address of client:"
        print self.addr
        print "Request details:"
        print(''.join(
            '< {line}\n'.format(line=line)
            for line in self.request.splitlines()
        ))
        print "End of details"

        self.parser.parse_requests(self.request)

        http_response = """\
HTTP/1.1 200 OK\r\n
Hello, World!
"""
        self.conn_socket.sendall(http_response)
        self.conn_socket.close()
        
    def run(self):
        """Run the thread of the connection handler"""
        self.handle_connection()
        

class Server:
    """HTTP Server"""

    def __init__(self, hostname, server_port, timeout):
        """Initialize the HTTP server
        
        Args:
            hostname (str): hostname of the server
            server_port (int): port that the server is listening on
            timeout (int): seconds until timeout
        """
        self.hostname = hostname
        self.server_port = server_port
        self.timeout = timeout
        self.done = False
    
    def run(self):
        """Run the HTTP Server and start listening"""
        listen_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        listen_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        listen_socket.bind((self.hostname, self.server_port))
        listen_socket.listen(1)
        while not self.done:
            client_conn, client_addr = listen_socket.accept()
            handler_conn = ConnectionHandler(client_conn, client_addr, self.timeout)
            handler_conn.run()
    
    def shutdown(self):
        """Safely shut down the HTTP server"""
        self.done = True
